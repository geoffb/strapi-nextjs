import React from 'react'
import { LoadingContainer } from 'css/theme/shared'

const Loading: React.FC = () => {
  return (
    <LoadingContainer>
      <img alt="Loading icon" src="loading.gif" />
    </LoadingContainer>
  )
}

export default Loading
