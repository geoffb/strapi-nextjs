import React from 'react'
import { default as NextLink } from 'next/link'
import { StyledLink } from 'css/theme/shared'

interface ILink {
  route: string
  text: string
}

export const Link: React.FC<ILink> = ({ route, text }) => {
  return (
    <NextLink href={route}>
      <StyledLink>{text}</StyledLink>
    </NextLink>
  )
}
