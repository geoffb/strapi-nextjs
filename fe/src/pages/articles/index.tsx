import React, { useEffect, useState } from 'react'
import { ApolloClient, gql, InMemoryCache } from '@apollo/client'
import { default as NextLink } from 'next/link'
import { Typography } from '@mui/material'
import { Container } from 'css/theme/shared'
import Link from 'components/Link'
import { Loading } from 'components/Loading'
import { apiConfig } from 'config/api'
import { routes } from 'config/routes'
import { ArticlesContainer, ArticleLink, ArticleBlock } from './styles'

const Articles: React.FC = () => {
  const [apiData, setApiData] = useState({ data: undefined, loading: true })
  const client = new ApolloClient({
    uri: apiConfig.uri,
    cache: new InMemoryCache(),
  })

  useEffect(() => {
    client
      .query({
        query: gql`
          query ($filters: ArticleFiltersInput!) {
            articles(filters: $filters, publicationState: LIVE) {
              data {
                id
                attributes {
                  slug
                  publishedAt
                }
              }
            }
          }
        `,
        variables: {
          filters: {},
        },
      })
      .then((response) => {
        // console.log(response)
        setApiData(response)
      })
  }, [])

  if (apiData.loading || !apiData.data) {
    return <Loading />
  }

  return (
    <Container>
      <Typography variant="h1">List of articles:</Typography>
      <ArticlesContainer>
        {apiData.data.articles.data.map((article) => (
          <ArticleBlock key={article.attributes.slug}>
            <NextLink href={`${routes.articles}/${article.id}`}>
              <ArticleLink variant="h2">
                <span>
                  {article.attributes.slug} [{article.attributes.publishedAt}]
                </span>
                <span>{'>'}</span>
              </ArticleLink>
            </NextLink>
          </ArticleBlock>
        ))}
      </ArticlesContainer>
      <Link route={routes.home} text="< Home" />
    </Container>
  )
}

export default Articles
