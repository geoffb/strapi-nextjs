import styled from '@emotion/styled'
import { Typography } from '@mui/material'

export const ArticlesContainer = styled.div`
  margin: 60px 0;
`

export const ArticleBlock = styled.div`
  cursor: pointer;
  border: 2px solid #666;
  padding: 10px;
  margin-top: 15px;

  :first-of-type {
    margin-top: 0;
  }
  
  :hover {
    background: #f1f1f1;
    border: 2px dashed #666;
  }
`

export const ArticleLink = styled(Typography)`
  display: flex;
  justify-content: space-between;
  gap: 30px;
`
