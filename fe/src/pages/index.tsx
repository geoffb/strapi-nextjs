import React from 'react'
import { Typography } from '@mui/material'
import { routes } from 'config/routes'
import { Container } from 'css/theme/shared'
import Link from 'components/Link'

const Home: React.FC = () => {
  return (
    <Container>
      <Typography variant="h1">🌴 Home 🌴</Typography>
      <Link route={routes.articles} text="Articles >" />
    </Container>
  )
}

export default Home
