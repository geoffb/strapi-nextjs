import { createTheme } from '@mui/material'
import { indigo, pink } from '@mui/material/colors'

const theme = createTheme({
  palette: {
    primary: {
      light: pink[300],
      main: pink[500],
      dark: pink[700],
    },
    secondary: {
      light: indigo[300],
      main: indigo[500],
      dark: indigo[700],
    },
  },
  typography: {
    fontFamily: ['Courier New', 'Courier', 'sans-serif'].join(','),
    h1: {
      fontSize: 35,
      fontWeight: 'bold',
      marginBottom: 30,
    },
    h2: {
      fontSize: 23,
    },
  },
})

export { theme }
