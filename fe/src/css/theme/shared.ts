import styled from '@emotion/styled'
import { Typography } from '@mui/material'

export const Container = styled.div`
  width: fit-content;
  margin: 0 auto;
  margin-top: 100px;
`

export const StyledLink = styled(Typography)`
  cursor: pointer;
  text-decoration: underline;
`

export const LoadingContainer = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`
